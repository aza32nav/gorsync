package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

const RSYNC = "rsync"
const ARG1 = "-rtvu"
const ARG2 = "--delete"

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Please provide a filename in the current directory")
		os.Exit(1)
	}

	filename := os.Args[1]

	byteslice, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
	}

	// Convert to string
	str := string(byteslice)

	// Split with \n
	origin, destination, folders := splitfile(str)

	fmt.Println("La informacion proporcionada por el archivo es:")

	fmt.Println("Origen:", origin)
	fmt.Println("Destino:", destination)
	fmt.Printf("Folders: %v\n", folders)
	fmt.Println("------------------------------------------------")

	for _, folder := range folders {
		if folder == "" {
			continue
		}

		cmdrsync := createCommandString(origin, destination, folder)
		fmt.Println("#################################################")
		fmt.Println(cmdrsync)
		log.Println("Inicia backup")
		output := runCommand(origin, destination, folder)
		fmt.Println(output)
		log.Println("Finaliza backup rsync")
		fmt.Println("+++++++++++++++++++++++++++++++++++++++++++++++++")
	}
}

func splitfile(strfile string) (string, string, []string) {
	dirs := strings.Split(strfile, "\n")
	origin := dirs[0]
	destination := dirs[1]
	folders := dirs[2:]
	return origin, destination, folders
}

func createCommandString(origin, destination, folder string) string {
	return RSYNC + " " + ARG1 + " " + ARG2 + " " + origin + folder + " " + destination + folder
}

func runCommand(origin, destination, folder string) string {
	var stdout bytes.Buffer
	originfolder := origin + folder
	destinationfolder := destination + folder
	cmd := exec.Command(RSYNC, ARG1, ARG2, originfolder, destinationfolder)
	cmd.Stdout = &stdout
	err := cmd.Run()
	if err != nil {
		log.Println("Error al ejecutar commando err:", err)
		os.Exit(1)
	}
	return stdout.String()
}
